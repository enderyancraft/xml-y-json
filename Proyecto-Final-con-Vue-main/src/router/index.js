import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '/registro',
    name: 'registro',
    component: () => import(/* webpackChunkName: "registrarse" */ '../views/Registro.vue')
  },
  {
    path: '/usuarios',
    name: 'usuarios',
    component: () => import(/* webpackChunkName: "usuarios" */ '../views/SesionUsuarios.vue')
  },
  {
    path: '/tipos',
    name: 'tipos',
    component: () => import(/* webpackChunkName: "tipos" */ '../views/tipo.vue')
  },

  {
    path: '/descargas',
    name: 'descargas',
    component: () => import(/* webpackChunkName: "descargas */ '../views/descargar')
  },

  

  {
    path: '/contactos',
    name: 'contactos',
    component: () => import(/* webpackChunkName: "contactanos" */ '../views/Contactanos.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router